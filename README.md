# tetris

![](https://user-images.githubusercontent.com/21541959/90352182-bcfcdf00-e06c-11ea-9f7d-75ba3c0ce042.png)

balok tetris dibuat dari karakter:

- A: clear
- B: block (dropping state)
- C: block (dropped state)

dari tetris font yang dibuat pakai:

[https://github.com/glyphr-studio/Glyphr-Studio-1](https://github.com/glyphr-studio/Glyphr-Studio-1)