package com.example.tetris

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val f = false //blank
    val t = true //block
    val blocks = arrayOf<BooleanArray>(
        booleanArrayOf(//0
            f, f, t, f,
            f, f, t, f,
            f, f, t, f,
            f, f, t, f
        ),
        booleanArrayOf(//1
            f, f, f, f,
            f, f, t, f,
            f, f, t, f,
            f, t, t, f
        ),
        booleanArrayOf(//2
            f, f, f, f,
            f, t, f, f,
            f, t, f, f,
            f, t, t, f
        ),
        booleanArrayOf(//3
            f, f, f, f,
            f, f, t, f,
            f, t, t, f,
            f, t, f, f
        ),
        booleanArrayOf(//4
            f, f, f, f,
            f, t, f, f,
            f, t, t, f,
            f, f, t, f
        ),
        booleanArrayOf(//5
            f, f, f, f,
            f, f, t, f,
            f, t, t, f,
            f, f, t, f
        ),
        booleanArrayOf(//6
            f, f, f, f,
            f, t, t, f,
            f, t, t, f,
            f, f, f, f
        )
    )

    val A = 'A' //blank
    val B = 'B' //tmp. block
    val C = 'C' //block
    val height = 23
    val width = 10

    val hndlr = Handler()
    val area = CharArray(height * width)
    var blck = blocks.random()
    val blckSz = blck.size
    val side = Math.sqrt(blckSz.toDouble()).toInt()

    var row = -1
    var col = (width - side) / 2 //center
    var delay = 999L
    var pause = true
    var start = false
    var skor = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clr()
        fDown()
    }
    private fun fDown(){
        if(!pause){
            row++
            if(chck(row, col)) put(row, col); else if(row==0){
                start = true
                pause = true
                pausBtn.text = "PLAY"
                areaView.text = "\ngameover\n"
            } else {
                repeat(area.size){ if(area[it]==B) area[it]=C }
                xxx()
                blck = blocks.random()
                row = -1
                col = (width - side) / 2 //center
                delay = 999L
            }
        }
        hndlr.postDelayed({fDown()}, delay)
    }
    fun fDrop(view: View){
        if(!pause) delay = 199L
    }
    fun fPaus(view: View){
        if(start){
            start = false
            clr()
            row = -1
        }
        pause = !pause
        pausBtn.text = if(pause) "PLAY" else "PAUS"
    }
    private fun clr(){
        area.fill(A)
        areaView.text = String(area.sliceArray(IntRange(3 * width, (height * width) - 1)))
    }
    fun fTurn(view: View){ //rotate right
        if(pause) return
        val oldBlck = blck
        val newBlck = BooleanArray(blck.size)
        repeat(blckSz){
            newBlck[it] = blck[(4*Math.abs((it%side)-side+1)) + (it/side)]
        }
        blck = newBlck
        if(chck(row, col)) put(row, col); else blck = oldBlck
    }
    fun fLeft(view: View){
        if(pause) return
        col--
        if(chck(row, col)) put(row, col); else col++
    }
    fun fRght(view: View){
        if(pause) return
        col++
        if(chck(row, col)) put(row, col); else col--
    }
    private fun put(row: Int, col: Int){
        repeat(area.size){if(area[it]==B) area[it]=A}
        var k = 0
        for(i in IntRange(row, row+3)){
            for(j in IntRange(col, col+3)){
                if(blck[k]) area[(i*width) + j] = B
                k++
            }
        }
        areaView.text = String(area.sliceArray(IntRange(3 * width, (height * width) - 1)))
    }
    private fun xxx(){ //remove block line
        var src = height - 1
        var dst = src
        while(src>0){
            for(i in IntRange(0, width-1)){
                if(area[(dst*width) + i]==A){
                    dst--
                    break
                }
            }
            src--
            if(src != dst) repeat(width){area[(dst*width) + it] = area[(src*width) + it]}
        }
        if(dst>0){
            skor += dst
            skorView.text = skor.toString()
        }
        while(dst>=0){
            repeat(width){area[(dst*width) + it] = A}
            dst--
        }
    }
    private fun chck(row: Int, col: Int): Boolean{
        var k = 0
        for(i in IntRange(row, row+side-1)){
            for(j in IntRange(col, col+side-1)){
                if(blck[k] && (i>=height || j<0 || j>=width || area[(i*width) + j]==C)) return false
                k++
            }
        }
        return true
    }
}


